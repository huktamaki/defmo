var colors = {
  yellow : [255,254,3],
  blue : [73,123,222],
  red : [228,20,16],
  white : [255,255,255],
  black : [12,14,9],
  all : [[228,20,16],[255,254,3],[73,123,222],[255,255,255],[12,14,9]]
}
var states = {
  menu : true,
  levels : false,
  about : false,
  play : false,
  level : 0,
  draw : null,
  solved : null,
  warning : null,
  break : null,
  pop : false,
  overlap : false,
}
var board = {
  size : null,
  center : null,
}
var texts = {
  font : null,
  title : 'DEFMO',
  about : 'DEFMO IS A PUZZLE\nVIDEO GAME.\nTHE GOAL IS TO DIVIDE\nSQUARES INTO\nNON-CONGRUENT\nRECTANGLES WITH THE\nSMALLEST POSSIBLE\nDIFFERENCE IN AREA\nBETWEEN THE LARGEST\nAND THE SMALLEST\nPIECES.',
  menu : ['START', 'ABOUT'],
  credit : 'AHARONOFF 2020',
  large : null,
  medium : null,
  small : null,
  weight : null,
  goal : [2,4,4,5,5,6,6,8,6,7,8,6,8,8,8,8,8,9,9,9,8,9,10,9,10],
  done : ['EXCELLENT', 'ACCEPTABLE', 'DREADFUL'],
  warning : 'NO TWO\nRECTANGLES\nSHOULD BE\nSHAPED\nTHE SAME',
  okay : '> OKAY, GOT IT <',
  again : '> TRY AGAIN <',
  next : '> NEXT ONE <',
}
var sounds = {
  bool : true,
  click : null,
  back : null,
  next : null,
  previous : null,
  start : null,
  end : null,
  excellent : null,
  acceptable : null,
  dreadful : null,
  warning : null,
}
var levels = {
  goal : [0,2,4,4,5,5,6,6,8,6,7,8,0],
  result : [0,0,0,0,0,0,0,0,0,0,0],
}
var rects = {
  upperleft : [],
  bottomright : [],
  colors : [],
  areas : [],
  rects : null,
  startX : null,
  startY : null,
  currX : null,
  currY : null,
  drawing : false,
  check : false,
}

function preload(){
  texts.font = loadFont('font/krona.ttf')
  sounds.click = loadSound('sounds/click.wav')
  sounds.back = loadSound('sounds/back.wav')
  sounds.next = loadSound('sounds/next.wav')
  sounds.previous = loadSound('sounds/previous.wav')
  sounds.start = loadSound('sounds/start.wav')
  sounds.end = loadSound('sounds/end.wav')
  sounds.excellent = loadSound('sounds/excellent.wav')
  sounds.acceptable = loadSound('sounds/acceptable.wav')
  sounds.dreadful = loadSound('sounds/dreadful.wav')
  sounds.warning = loadSound('sounds/warning.wav')
}

function setup(){
  createCanvas(windowWidth, windowHeight)
  board.size = round(windowHeight * 0.9)
  board.center = round(windowHeight * 0.9 * 0.5)
  texts.large = round(windowHeight * 0.9 * 0.2)
  texts.medium = round(windowHeight * 0.9 * 0.125)
  texts.small = round(windowHeight * 0.9 * 0.05)
  texts.weight = round(windowHeight * 0.9 * 0.05)
}

function draw(){
  createCanvas(board.size, board.size)
  background(255)
  colorMode(RGB, 255, 255, 255, 1)
  rectMode(CENTER)
  textAlign(CENTER)
  textFont(texts.font)
  // MENU
  if(states.menu === true && states.levels === false && states.about === false && states.play === false){
    // title
    textSize(texts.large)
    stroke(colors.black)
    strokeWeight(texts.weight)
    for(var i = 0; i < texts.title.length; i++){
      fill(colors.all[i])
      text(texts.title[i], board.size * 0.15 + i * 0.175 * board.size, board.size * 0.25)
    }
    // menu items
    for(var i = 0; i < texts.menu.length; i++){
      fill(colors.black)
      textSize(texts.medium)
      noStroke()
      text(texts.menu[i], board.center, board.size * 0.5 + i * 0.2 * board.size)
    }
    if(mouseX >= board.size * 0.2 && mouseX <= board.size * 0.8 && mouseY >= board.size * 0.4 && mouseY <= board.size * 0.5){
      stroke(colors.black)
      strokeWeight(texts.weight * 0.25)
      text(texts.menu[0], board.center, board.size * 0.5)
    }
    if(mouseX >= board.size * 0.2 && mouseX <= board.size * 0.8 && mouseY >= board.size * 0.6 && mouseY <= board.size * 0.7){
      stroke(colors.black)
      strokeWeight(texts.weight * 0.25)
      text(texts.menu[1], board.center, board.size * 0.7)
    }
    // credit
    fill(colors.black)
    noStroke()
    textSize(texts.small)
    text(texts.credit, board.center, board.size * 0.9)
  }
  // LEVELS
  if(states.menu === false && states.levels === true && states.about === false && states.play === false){
    fill(colors.black)
    noStroke()
    textSize(texts.medium)
    text('<', board.size * 0.1, board.size * 0.15)
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
      stroke(colors.black)
      strokeWeight(texts.weight * 0.25)
      text('<', board.size * 0.1, board.size * 0.15)
    }
    noStroke()
    fill(colors.black)
    rect(board.center, board.center, board.center, board.center)
    if(states.level >= 1){
      rect(board.size * 0.125, board.center, board.size * 0.2, board.size * 0.2)
    }
    if(states.level <= 9){
      rect(board.size * 0.875, board.center, board.size * 0.2, board.size * 0.2)
    }
    textSize(texts.large)
    noStroke()
    fill(colors.white)
    text(states.level + 1, board.center, board.center + texts.large * 0.4)
    textSize(texts.large * 0.25)
    text(texts.goal[states.level], board.size * 0.65, board.size * 0.35)
    if(levels.result[states.level] === 0){
      text('?', board.size * 0.65, board.size * 0.675)
    } else {
      text(levels.result[states.level], board.size * 0.65, board.size * 0.675)
    }
    if(mouseX >= board.size * 0.25 && mouseX <= board.size * 0.75 && mouseY >= board.size * 0.25 && mouseY <= board.size * 0.75){
      noFill()
      stroke(colors.white)
      strokeWeight(texts.weight * 0.5)
      rect(board.center, board.center, board.size * 0.45, board.size * 0.45)
    }
    textSize(texts.small)
    if(states.level >= 1){
      noStroke()
      fill(colors.white)
      text(states.level, board.size * 0.125, board.center + texts.small * 0.4)
      if(mouseX >= board.size * 0.025 && mouseX <= board.size * 0.225 && mouseY >= board.size * 0.4 && mouseY <= board.size * 0.6){
        noFill()
        stroke(colors.white)
        strokeWeight(texts.weight * 0.25)
        rect(board.size * 0.125, board.center, board.size * 0.175, board.size * 0.175)
      }
    }
    if(states.level <= 9){
      noStroke()
      fill(colors.white)
      text(states.level + 2, board.size * 0.875, board.center + texts.small * 0.4)
      if(mouseX >= board.size * 0.775 && mouseX <= board.size * 0.975 && mouseY >= board.size * 0.4 && mouseY <= board.size * 0.6){
        noFill()
        stroke(colors.white)
        strokeWeight(texts.weight * 0.25)
        rect(board.size * 0.875, board.center, board.size * 0.175, board.size * 0.175)
      }
    }
  }
  // ABOUT
  if(states.menu === false && states.levels === false && states.about === true && states.play === false){
    fill(colors.black)
    noStroke()
    textSize(texts.medium)
    text('<', board.size * 0.1, board.size * 0.15)
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
      stroke(colors.black)
      strokeWeight(texts.weight * 0.25)
      text('<', board.size * 0.1, board.size * 0.15)
    }
    textAlign(LEFT)
    noStroke()
    textSize(texts.small)
    text(texts.about, board.size * 0.1, board.size * 0.25)
  }
  // PLAY
  if(states.menu === false && states.levels === false && states.about === false && states.play === true){
    fill(colors.black)
    noStroke()
    textSize(texts.medium)
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
      stroke(colors.black)
      strokeWeight(texts.weight * 0.25)
    } else {
      noStroke()
    }
    text('<', board.size * 0.1, board.size * 0.15)
    if(mouseX >= board.size * 0.85 && mouseX <= board.size * 0.95 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
      stroke(colors.black)
      strokeWeight(texts.weight * 0.25)
    } else {
      noStroke()
    }
    text('>', board.size * 0.9, board.size * 0.15)
    if(mouseX >= board.size * 0.85 && mouseX <= board.size * 0.95 && mouseY >= board.size * 0.85 && mouseY <= board.size * 0.95){
      stroke(colors.black)
      strokeWeight(texts.weight * 0.25)
    } else {
      noStroke()
    }
    text('×', board.size * 0.9, board.size * 0.95)
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.85 && mouseY <= board.size * 0.95){
      stroke(colors.black)
      strokeWeight(texts.weight * 0.25)
    } else {
      noStroke()
    }
    text('-', board.size * 0.1, board.size * 0.95)
    noStroke()
    text(levels.goal[states.level + 1], board.center, board.size * 0.125)
    // draw board
    for(var i = 0; i < states.level + 3; i++){
      for(var j = 0; j < states.level + 3; j++){
        if(states.level % 2 === 0){
          fill(colors.white)
          stroke(colors.black)
          strokeWeight(round(levels.goal.length / (states.level + 3)) * board.size * 0.006)
          rect(board.center + (i - floor((states.level + 3) * 0.5)) * board.size * 0.65 * (1 / (states.level + 3)), board.center + (j - floor((states.level + 3) * 0.5)) * board.size * 0.65 * (1 / (states.level + 3)), board.size * 0.65 * (1 / (states.level + 3)), board.size * 0.65 * (1 / (states.level + 3)))
        } else {
          fill(colors.white)
          stroke(colors.black)
          strokeWeight(round(levels.goal.length / (states.level + 3)) * board.size * 0.006)
          rect(board.center + (i - floor((states.level + 3) * 0.5) + 0.5) * board.size * 0.65 * (1 / (states.level + 3)), board.center + (j - floor((states.level + 3) * 0.5) + 0.5) * board.size * 0.65 * (1 / (states.level + 3)), board.size * 0.65 * (1 / (states.level + 3)), board.size * 0.65 * (1 / (states.level + 3)))
        }
      }
    }
    // draw rectangles
    for(var i = 0; i < rects.bottomright.length; i++){
      fill(colors.all[rects.colors[rects.colors.length - rects.bottomright.length + i]])
      stroke(colors.black)
      strokeWeight(round(levels.goal.length / (states.level + 3)) * board.size * 0.006)
      rect(board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) + 0.5 * board.size * 0.65 * (1 / (states.level + 3)) + (rects.bottomright[i][0] - (rects.bottomright[i][0] - rects.upperleft[i][0]) * 0.5) * board.size * 0.65 * (1 / (states.level + 3)), board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) + 0.5 * board.size * 0.65 * (1 / (states.level + 3)) + (rects.bottomright[i][1] - (rects.bottomright[i][1] - rects.upperleft[i][1]) * 0.5) * board.size * 0.65 * (1 / (states.level + 3)), (rects.bottomright[i][0] - rects.upperleft[i][0] + 1) * board.size * 0.65 * (1 / (states.level + 3)) - round(levels.goal.length / (states.level + 3)) * board.size * 0.006, (rects.bottomright[i][1] - rects.upperleft[i][1] + 1) * board.size * 0.65 * (1 / (states.level + 3)) - round(levels.goal.length / (states.level + 3)) * board.size * 0.006)
    }
    // solved pop up window
    if(states.solved === true){
      if(floor(frameCount / 60) % 2 === 0){
        fill(colors.white)
        stroke(colors.black)
        strokeWeight(round(levels.goal.length / (states.level + 3)) * board.size * 0.006)
        textSize(texts.small)
        rect(board.center, board.center, board.size * 0.55, board.size * 0.3)
        fill(colors.black)
        noStroke()
        if(levels.result[states.level] - levels.goal[states.level + 1] === 0){
          text(texts.done[0], board.center, board.size * 0.525)
        }
        if(levels.result[states.level] - levels.goal[states.level + 1] > 0 && levels.result[states.level] - levels.goal[states.level + 1] <= 3){
          text(texts.done[1], board.center, board.size * 0.525)
        }
        if(levels.result[states.level] - levels.goal[states.level + 1] > 3){
          text(texts.done[2], board.center, board.size * 0.525)
        }
      }
    }
    // congruence pop up window
    if(states.warning === true){
      fill(colors.white)
      stroke(colors.black)
      strokeWeight(round(levels.goal.length / (states.level + 3)) * board.size * 0.006)
      rect(board.center, board.center, board.size * 0.55, board.size * 0.5)
      fill(colors.black)
      noStroke()
      textSize(texts.small)
      text(texts.warning, board.center, board.size * 0.4)
    }
    // drawing rectangle while mouse is dragged
    if(rects.drawing === true && mouseX >= board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) && mouseX <= board.size * 0.5 + (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) && mouseY >= board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) && mouseY <= board.size * 0.5 + (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3))){
      rectMode(CORNERS)
      fill(colors.all[floor(frameCount / 60) % colors.all.length])
      stroke(colors.black)
      strokeWeight(round(levels.goal.length / (states.level + 3)) * board.size * 0.006)
      if(states.level % 2 === 0){
        rect(
          board.center + (board.size * 0.65 * (1 / (states.level + 3))) * floor((rects.startX - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))) - floor((states.level + 3) * 0.5)) - 0.5 * board.size * 0.65 * (1 / (states.level + 3)) + board.size * 0.65 * (1 / (states.level + 3)) * 0.2,
          board.center + (board.size * 0.65 * (1 / (states.level + 3))) * floor((rects.startY - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))) - floor((states.level + 3) * 0.5)) - 0.5 * board.size * 0.65 * (1 / (states.level + 3)) + board.size * 0.65 * (1 / (states.level + 3)) * 0.2,
          board.center + (board.size * 0.65 * (1 / (states.level + 3))) * floor((mouseX - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))) - floor((states.level + 3) * 0.5)) + 0.5 * board.size * 0.65 * (1 / (states.level + 3)) + board.size * 0.65 * (1 / (states.level + 3)) * 0.2,
          board.center + (board.size * 0.65 * (1 / (states.level + 3))) * floor((mouseY - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))) - floor((states.level + 3) * 0.5)) + 0.5 * board.size * 0.65 * (1 / (states.level + 3)) + board.size * 0.65 * (1 / (states.level + 3)) * 0.2
        )
      } else {
        rect(
          board.center + (board.size * 0.65 * (1 / (states.level + 3))) * floor((rects.startX - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))) - floor((states.level + 3) * 0.5)) + board.size * 0.65 * (1 / (states.level + 3)) * 0.2,
          board.center + (board.size * 0.65 * (1 / (states.level + 3))) * floor((rects.startY - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))) - floor((states.level + 3) * 0.5)) + board.size * 0.65 * (1 / (states.level + 3)) * 0.2,
          board.center + (board.size * 0.65 * (1 / (states.level + 3))) * floor((mouseX - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))) - floor((states.level + 3) * 0.5)) + board.size * 0.65 * (1 / (states.level + 3)) + board.size * 0.65 * (1 / (states.level + 3)) * 0.2,
          board.center + (board.size * 0.65 * (1 / (states.level + 3))) * floor((mouseY - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))) - floor((states.level + 3) * 0.5)) + board.size * 0.65 * (1 / (states.level + 3)) + board.size * 0.65 * (1 / (states.level + 3)) * 0.2
        )
      }
      rectMode(CENTER)
    }
  }
}

function mousePressed(){
  // MENU
  if(states.menu === true && states.levels === false && states.about === false && states.play === false){
    if(mouseX >= board.size * 0.2 && mouseX <= board.size * 0.8 && mouseY >= board.size * 0.4 && mouseY <= board.size * 0.5){
      setTimeout(function(){states.menu = false; states.levels = true; states.about = false; states.play = false}, 1)
      playSound(sounds.click)
    }
    if(mouseX >= board.size * 0.2 && mouseX <= board.size * 0.8 && mouseY >= board.size * 0.6 && mouseY <= board.size * 0.7){
      states.menu = false
      states.levels = false
      states.about = true
      states.play = false
      playSound(sounds.click)
    }
  }
  // LEVELS
  if(states.menu === false && states.levels === true && states.about === false && states.play === false){
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
      states.menu = true
      states.levels = false
      states.about = false
      states.play = false
      playSound(sounds.back)
    }
    if(mouseX >= board.size * 0.25 && mouseX <= board.size * 0.75 && mouseY >= board.size * 0.25 && mouseY <= board.size * 0.75 && states.level === 0){
      rects.upperleft = []
      rects.bottomright = []
      setTimeout(function(){states.draw = true; states.menu = false; states.levels = false; states.about = false; states.play = true; states.warning = false; states.solved = false}, 1)
      rects.rects = create2DArray(states.level + 3, states.level + 3, 0, true)
      rects.check = create2DArray(states.level + 3, states.level + 3, 0, true)
      rects.areas = []
      playSound(sounds.click)
    } else {
      playSound(sounds.warning)
    }
    if(mouseX >= board.size * 0.25 && mouseX <= board.size * 0.75 && mouseY >= board.size * 0.25 && mouseY <= board.size * 0.75 && levels.result[states.level - 1] - levels.goal[states.level] <= 3 && levels.result[states.level - 1] > 0){
      rects.upperleft = []
      rects.bottomright = []
      setTimeout(function(){states.draw = true; states.menu = false; states.levels = false; states.about = false; states.play = true; states.warning = false; states.solved = false}, 1)
      rects.rects = create2DArray(states.level + 3, states.level + 3, 0, true)
      rects.check = create2DArray(states.level + 3, states.level + 3, 0, true)
      rects.areas = []
      playSound(sounds.click)
    }
    if(states.level >= 1){
      if(mouseX >= board.size * 0.025 && mouseX <= board.size * 0.225 && mouseY >= board.size * 0.4 && mouseY <= board.size * 0.6){
        states.level--
        playSound(sounds.previous)
      }
    }
    if(states.level <= 9){
      if(mouseX >= board.size * 0.775 && mouseX <= board.size * 0.975 && mouseY >= board.size * 0.4 && mouseY <= board.size * 0.6){
        states.level++
        playSound(sounds.next)
      }
    }
  }
  // ABOUT
  if(states.menu === false && states.levels === false && states.about === true && states.play === false){
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
      states.menu = true
      states.levels = false
      states.about = false
      states.play = false
      playSound(sounds.back)
    }
  }
  // PLAY
  if(states.menu === false && states.levels === false && states.about === false && states.play === true){
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
      setTimeout(function(){states.menu = false; states.levels = true; states.about = false; states.play = false}, 1)
      states.draw = false
      playSound(sounds.back)
    }
    if(mouseX >= board.size * 0.85 && mouseX <= board.size * 0.95 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
      playSound(sounds.warning)
    }
    if(mouseX >= board.size * 0.85 && mouseX <= board.size * 0.95 && mouseY >= board.size * 0.85 && mouseY <= board.size * 0.95 && rects.rects.flat().every(startedness) === false){
      setTimeout(function(){states.draw = true; states.menu = false; states.levels = false; states.about = false; states.play = true; states.warning = false; states.solved = false}, 1)
      rects.rects = create2DArray(states.level + 3, states.level + 3, 0, true)
      rects.areas = []
      rects.upperleft = []
      rects.bottomright = []
      states.pop = false
      playSound(sounds.click)
      playSound(sounds.back)
    }
    if(mouseX >= board.size * 0.85 && mouseX <= board.size * 0.95 && mouseY >= board.size * 0.85 && mouseY <= board.size * 0.95 && rects.rects.flat().every(startedness) === true){
      playSound(sounds.warning)
    }
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.85 && mouseY <= board.size * 0.95 && rects.bottomright.length > 0 && rects.upperleft.length > 0){
      if(states.overlap === false){
        for(var i = 0; i < rects.bottomright[rects.bottomright.length - 1][0] - rects.upperleft[rects.upperleft.length - 1][0] + 1; i++){
          for(var j = 0; j < rects.bottomright[rects.bottomright.length - 1][1] - rects.upperleft[rects.upperleft.length - 1][1] + 1; j++){
            rects.rects[rects.upperleft[rects.upperleft.length - 1][0] + i][rects.upperleft[rects.upperleft.length - 1][1] + j] = 0
          }
        }
        rects.upperleft.pop()
        rects.bottomright.pop()
        rects.areas.pop()
        states.warning = false
        states.draw = true
        states.pop = false
        states.overlap = false
        playSound(sounds.click)
        setTimeout(function(){states.draw = true}, 1)
        if(rects.rects.flat().every(checkArray) === false && states.warning !== true){
          states.solved = false
        }
      }
    }
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.85 && mouseY <= board.size * 0.95 && states.warning === true){
      if(states.overlap === false){
        for(var i = 0; i < rects.bottomright[rects.bottomright.length - 1][0] - rects.upperleft[rects.upperleft.length - 1][0] + 1; i++){
          for(var j = 0; j < rects.bottomright[rects.bottomright.length - 1][1] - rects.upperleft[rects.upperleft.length - 1][1] + 1; j++){
            rects.rects[rects.upperleft[rects.upperleft.length - 1][0] + i][rects.upperleft[rects.upperleft.length - 1][1] + j] = 0
          }
        }
        rects.upperleft.pop()
        rects.bottomright.pop()
        states.warning = false
        states.draw = true
        states.pop = false
        playSound(sounds.click)
        setTimeout(function(){states.draw = true}, 1)
      } else {
        states.warning = false
        states.draw = true
        states.pop = false
        states.overlap = false
        playSound(sounds.click)
        setTimeout(function(){states.draw = true}, 1)
      }
    }
    if(states.draw === true){
      if(mouseX >= board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) && mouseX <= board.size * 0.5 + (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) && mouseY >= board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) && mouseY <= board.size * 0.5 + (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3))){
        if(rects.rects[floor((mouseX - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))))][floor((mouseY - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))))] !== 1){
          rects.upperleft.push([floor((mouseX - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3)))), floor((mouseY - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))))])
        }
        playSound(sounds.start)
        frameCount = 0
        rects.drawing = true
        rects.startX = mouseX
        rects.startY = mouseY
      }
    }
    if(mouseX >= board.size * 0.05 && mouseX <= board.size * 0.15 && mouseY >= board.size * 0.85 && mouseY <= board.size * 0.95 && states.warning === false){
      playSound(sounds.warning)
    }
    if(states.solved === true && levels.result[states.level] - levels.goal[states.level + 1] <= 3 && levels.result[states.level] > 0){
      if(mouseX >= board.size * 0.85 && mouseX <= board.size * 0.95 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
        states.level++
        rects.upperleft = []
        rects.bottomright = []
        setTimeout(function(){states.draw = true; states.menu = false; states.levels = false; states.about = false; states.play = true; states.warning = false; states.solved = false}, 1)
        rects.rects = create2DArray(states.level + 3, states.level + 3, 0, true)
        rects.areas = []
        playSound(sounds.click)
        playSound(sounds.next)
      }
    }
    if(states.level === 0 && levels.result[states.level] - levels.goal[states.level + 1] <= 3 && levels.result[states.level] > 0){
      if(mouseX >= board.size * 0.85 && mouseX <= board.size * 0.95 && mouseY >= board.size * 0.05 && mouseY <= board.size * 0.15){
        states.level++
        rects.upperleft = []
        rects.bottomright = []
        setTimeout(function(){states.draw = true; states.menu = false; states.levels = false; states.about = false; states.play = true; states.warning = false; states.solved = false}, 1)
        rects.rects = create2DArray(states.level + 3, states.level + 3, 0, true)
        rects.areas = []
        playSound(sounds.click)
        playSound(sounds.next)
      }
    }
  }
}

function mouseDragged(){
  rects.currX = mouseX
  rects.currY = mouseY
}

function mouseReleased(){
  if(mouseX >= board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) && mouseX <= board.size * 0.5 + (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) && mouseY >= board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)) && mouseY <= board.size * 0.5 + (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3))){
    if(states.draw === true && rects.upperleft.length - rects.bottomright.length === 1){
      if(rects.rects[floor((mouseX - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))))][floor((mouseY - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))))] !== 1){
        rects.bottomright.push([floor((mouseX - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3)))), floor((mouseY - (board.size - board.size * 0.5 - (states.level + 3) * 0.5 * board.size * 0.65 * (1 / (states.level + 3)))) / (board.size * 0.65 * (1 / (states.level + 3))))])
        if(rects.bottomright.length >= 2){
          if(rects.bottomright[rects.bottomright.length - 1][0] < rects.upperleft[rects.upperleft.length - 1][0] || rects.bottomright[rects.bottomright.length - 1][1] < rects.upperleft[rects.bottomright.length - 1][1]){
            for(var i = 0; i < rects.bottomright[rects.bottomright.length - 2][0] - rects.upperleft[rects.upperleft.length - 2][0] + 1; i++){
              for(var j = 0; j < rects.bottomright[rects.bottomright.length - 2][1] - rects.upperleft[rects.upperleft.length - 2][1] + 1; j++){
                rects.rects[rects.upperleft[rects.upperleft.length - 2][0] + i][rects.upperleft[rects.upperleft.length - 2][1] + j] = 0
              }
            }
            rects.bottomright.pop()
            rects.upperleft.pop()
          }
        }
        if(rects.bottomright.length < 2){
          if(rects.bottomright[rects.bottomright.length - 1][0] < rects.upperleft[rects.upperleft.length - 1][0] || rects.bottomright[rects.bottomright.length - 1][1] < rects.upperleft[rects.bottomright.length - 1][1]){
            rects.bottomright = []
            rects.upperleft = []
          }
        }
        for(var i = 0; i < rects.bottomright.length - 1; i++){
          if((rects.bottomright[i][0] - rects.upperleft[i][0] === rects.bottomright[rects.bottomright.length - 1][0] - rects.upperleft[rects.upperleft.length - 1][0] && rects.bottomright[i][1] - rects.upperleft[i][1] === rects.bottomright[rects.bottomright.length - 1][1] - rects.upperleft[rects.upperleft.length - 1][1]) || (rects.bottomright[i][0] - rects.upperleft[i][0] === rects.bottomright[rects.bottomright.length - 1][1] - rects.upperleft[rects.upperleft.length - 1][1] && rects.bottomright[i][1] - rects.upperleft[i][1] === rects.bottomright[rects.bottomright.length - 1][0] - rects.upperleft[rects.upperleft.length - 1][0])){
            states.warning = true
            states.pop = true
          }
        }
        if(states.pop === true){
          states.draw = false
          playSound(sounds.warning)
        }
        rects.colors.push((floor(frameCount / 60) % colors.all.length))
        if(rects.bottomright.length >= 1 && rects.upperleft.length >= 1){
          rects.check = create2DArray(rects.bottomright[rects.bottomright.length - 1][0] - rects.upperleft[rects.upperleft.length - 1][0] + 1, rects.bottomright[rects.bottomright.length - 1][1] - rects.upperleft[rects.upperleft.length - 1][1] + 1, 0, true)
          for(var i = 0; i < rects.check.length; i++){
            for(var j = 0; j < rects.check[i].length; j++){
              rects.check[i][j] = rects.rects[rects.upperleft[rects.upperleft.length - 1][0] + i][rects.upperleft[rects.upperleft.length - 1][1] + j]
            }
          }
          if(rects.check.flat().every(startedness) === false){
            rects.upperleft.pop()
            rects.bottomright.pop()
            states.pop = false
            states.overlap = true
            playSound(sounds.warning)
          } else {
            for(var i = 0; i < rects.bottomright[rects.bottomright.length - 1][0] - rects.upperleft[rects.upperleft.length - 1][0] + 1; i++){
              for(var j = 0; j < rects.bottomright[rects.bottomright.length - 1][1] - rects.upperleft[rects.upperleft.length - 1][1] + 1; j++){
                rects.rects[rects.upperleft[rects.upperleft.length - 1][0] + i][rects.upperleft[rects.upperleft.length - 1][1] + j] = 1
              }
            }
            if(rects.bottomright[rects.bottomright.length - 1][0] >= rects.upperleft[rects.upperleft.length - 1][0] && rects.bottomright[rects.bottomright.length - 1][1] >= rects.upperleft[rects.bottomright.length - 1][1]){
              rects.areas.push((rects.bottomright[rects.bottomright.length - 1][0] - rects.upperleft[rects.upperleft.length - 1][0] + 1) * (rects.bottomright[rects.bottomright.length - 1][1] - rects.upperleft[rects.upperleft.length - 1][1] + 1))
            }
            playSound(sounds.end)
            rects.startX = 0
            rects.startY = 0
            rects.currX = 0
            rects.currY = 0
            rects.drawing = false
          }
        }
      } else {
        rects.upperleft.pop()
      }
      if(rects.rects.flat().every(checkArray) === true && states.warning !== true){
        if(rects.bottomright.length === 1){
          levels.result[states.level] = (states.level + 3) * (states.level + 3)
        } else {
          levels.result[states.level] = max(rects.areas) - min(rects.areas)
        }
        states.solved = true
        frameCount = 0
        if(levels.result[states.level] - levels.goal[states.level + 1] === 0){
          playSound(sounds.excellent)
        }
        if(levels.result[states.level] - levels.goal[states.level + 1] > 0 && levels.result[states.level] - levels.goal[states.level + 1] <= 3){
          playSound(sounds.acceptable)
        }
        if(levels.result[states.level] - levels.goal[states.level + 1] > 3){
          playSound(sounds.dreadful)
        }
      }
    }
  } else {
    if(rects.upperleft.length - rects.bottomright.length === 1){
      rects.upperleft.pop()
    }
  }
  rects.drawing = false
}

function playSound(s){
  if(sounds.bool === true){
    s.play()
  }
}
function create2DArray(numRows, numCols, init, bool){
  var array = [];
  for(var i = 0; i < numRows; i++){
    var columns = []
    for(var j = 0; j < numCols; j++){
      if(bool === true){
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
function checkArray(a){
  return a > 0
}
function startedness(a){
  return a < 1
}
function windowResized(){
  createCanvas(windowWidth, windowHeight)
  board.size = round(windowHeight * 0.9)
  board.center = round(windowHeight * 0.9 * 0.5)
  texts.large = round(windowHeight * 0.9 * 0.2)
  texts.medium = round(windowHeight * 0.9 * 0.125)
  texts.small = round(windowHeight * 0.9 * 0.05)
  texts.weight = round(windowHeight * 0.9 * 0.05)
}
